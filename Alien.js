var Alien = {

};



Alien = function (game, key) {
	console.log("alien added");

    Phaser.Sprite.call(this, game,  2 /game.width, -100, key);

    this.texture.baseTexture.scaleMode = PIXI.scaleModes.NEAREST;

    this.anchor.set(0.5);

    this.checkWorldBounds = true;
    this.outOfBoundsKill = true;
	 this.health = 10;

    this.points = {
        'x': [ 0, 128, 256, 384, 512, 650,960 ],
        'y': [ 330, 90, 269, 101, 381, 214,400 ]
    };

	 this.pi = 0;
	 this.path = [];

	 this.plot();



};

Alien.prototype = Object.create(Phaser.Sprite.prototype);
Alien.prototype.constructor = Alien;



Alien.prototype.plot = function () {

   console.log("plot this");

    this.path = [];

    var x = 2 /game.width;

    for (var i = 0; i <= 1; i += x)
    {

            this.px = game.math.catmullRomInterpolation(this.points.y, i);
            this.py = game.math.catmullRomInterpolation(this.points.x, i);


        this.path.push( { x: this.px, y: this.py });


    };



},


Alien.prototype.update = function () {


   this.x = this.path[this.pi].x;
	this.y = this.path[this.pi].y;

	this.pi++;
	if(this.pi >= this.path.length){
		this.pi = 0;
		this.kill();
	}


};
