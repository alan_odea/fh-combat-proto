var game;
var bullets;
var nextFire = 0;
var ship,turret;
var controlShip = false;
var x;
var y;
var pi,points,path;
var weapon1,weapon2;
var enemies;
var alien;
var bulletOffset;
var bulletSpeed;
var bulletFireRate;
var bulletAngle;
var explosions;

window.onload = function() {
	game = new Phaser.Game(960, 1680, Phaser.AUTO, "");
		game.state.add("Boot", boot);
    game.state.add("Preload", preload);
    game.state.add("TitleScreen", titleScreen);
    game.state.add("PlayGame", playGame);
    // game.state.add("GameOverScreen", gameOverScreen);
    game.state.start("Boot");
}

var boot = function(game){};
boot.prototype = {
  	preload: function(){
          this.game.load.image("loading","loading.png");

	},
  	create: function(){
		game.scale.pageAlignHorizontally = true;
		game.scale.pageAlignVertically = true;
    game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
		this.game.state.start("Preload");
	}
}

var preload = function(game){};
preload.prototype = {
	preload: function(){
    var loadingBar = this.add.sprite(game.width / 2, game.height / 2, "loading");
      loadingBar.anchor.setTo(0.5);
      	game.load.setPreloadSprite(loadingBar);
				game.load.image('console_player', 'console_player.png');
				game.load.image('ship_player', 'ship_player.png');
				game.load.image('turret_player', 'turret_player.png');
				game.load.image('console_enemy', 'console_enemy.png');
				game.load.image('ship_enemy', 'ship_enemy.png');
				game.load.spritesheet('kaboom', 'explosion.png', 80, 80);
				game.load.spritesheet('bigKaboom', 'explosjon3.png', 120, 120);

          for (var i = 1; i <= 11; i++)
          {
              this.load.image('bullet' + i, 'bullet' + i + '.png');
          }
	},
  	create: function(){
		this.game.state.start("TitleScreen");
	}
}

var titleScreen = function(game){};
titleScreen.prototype = {
     create: function(){

		  	game.renderer.renderSession.roundPixels = true;
				game.stage.backgroundColor = "#000000";

				console.log("console screen");
				this.startGame();

     },

	  update:function(){

	  },
     startGame: function(){

          this.game.state.start("PlayGame");
     }
}

/*============GAME=====================*/

var playGame = function(game){};
playGame.prototype = {
     create: function(){

		  //initialise bullet variables
		  		bulletOffset = 0;
		  	 	bulletSpeed = 400;
		  	 	bulletFireRate = 560;
		  	 	bulletAngle = 10;

		  	/*=====Create two different weapons======*/
				 weapon1 = game.add.weapon(30, 'bullet1');
				 	weapon1.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
					weapon1.bulletAngleOffset = bulletOffset;
					weapon1.bulletSpeed = bulletSpeed+200;
					weapon1.fireRate = bulletFireRate-500;
					weapon1.bulletAngleVariance = 10;

					weapon2 = game.add.weapon(30, 'bullet5');
						weapon2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
			    	weapon2.bulletAngleOffset = bulletOffset;
			    	weapon2.bulletSpeed = bulletSpeed;
						weapon2.fireRate = bulletFireRate-500;


				 /*=====create a player ship=======*/
			  ship = this.add.sprite(320, 500, 'ship_player');
				 //ship.anchor.setTo(0.5);
				 turret = this.add.sprite(ship.x, ship.y, 'turret_player');

				 turret.scale.setTo(1,1);
				 turret.anchor.setTo(0.5);
			  game.physics.arcade.enable(ship);


				 /*====position weapons relative to player ship======*/
			    weapon1.trackSprite(ship, ship.width*0.5, ship.height-60);
				 weapon2.trackSprite(turret, 0,0,true);

			    cursors = this.input.keyboard.createCursorKeys();
			    fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

				 /*====create some enemies=====*/

				 this.enemyTime = game.time.now + 2000;
				 enemies = game.add.group();
				 enemies.enableBody = true;
				 enemies.physicsBodyType = Phaser.Physics.ARCADE;

				 explosions = game.add.group();
				 explosions.createMultiple(30, 'kaboom');
				 explosions.forEach(this.setupInvader, this);

				 bigExplosions = game.add.group();
				 bigExplosions.createMultiple(30, 'bigKaboom');
				 bigExplosions.forEach(this.setupInvader, this);

     },






		update:function(){
			/*=======Detect hits between enemies and player bullets========*/
			this.physics.arcade.overlap(weapon1.bullets, enemies, this.killEnemy, null, this);
			this.physics.arcade.overlap(weapon2.bullets, enemies, this.killEnemy, null, this);

				/*=========Control ship relative to mouse position=========*/
							var speed = 10;
							var yDistance = game.input.y - ship.y;
							var xDistance = game.input.x - ship.x;

							if (Math.sqrt(yDistance*yDistance +  xDistance*xDistance) < speed)
							{
								ship.x = game.input.x;
								ship.y = game.input.y;
							}
							else
							{
								var radian = Math.atan2(yDistance, xDistance);
								ship.x += Math.cos(radian) * speed;
								ship.y += (Math.sin(radian) * speed);
								rotation = radian * 180 / Math.PI;
							}
							turret.x = ship.x + ship.width*0.5;
							turret.y = ship.y + ship.height*0.5;
							turret.rotation += 0.25;

							/*====Auto fire weapons====*/
							weapon1.fire();
							weapon2.fire();


			             /*=====Add more enemies after time====*/
							if (this.game.time.now > this.enemyTime)
								{
										this.enemyTime = game.time.now + 1700;
								      this.addAliens();
								}


		},

      addAliens:function(){

    	      alien = new Alien(this.game, 'ship_enemy');
    	      enemies.add(alien);

      },

		setupInvader:function (invader) {

		    invader.anchor.x = 0.5;
		    invader.anchor.y = 0.5;
		    invader.animations.add('kaboom');

		},



		killEnemy:function(bullet,enemy){
			console.log("kill both");
			var explosion = explosions.getFirstExists(false);
			    explosion.reset(enemy.body.x+enemy.width*0.5, enemy.body.y+enemy.height*0.5);
			    explosion.play('kaboom', 30, false, true);

			/*===which bullet hit the enemy====*/
			console.log(bullet.key);
			/*===set bullet damage according to bullet key=====*/
			var damage;

			switch(bullet.key) {
			    case 'bullet1':
					 damage = 1;
			        break;
 			    case 'bullet5':
 					 damage = 5;
 			        break;
				  }

			bullet.kill();
			enemy.health -= damage;
			if(enemy.health <=0)
				{
					var explosion = bigExplosions.getFirstExists(false);
					    explosion.reset(enemy.body.x+enemy.width*0.5, enemy.body.y+enemy.height*0.5);
					    explosion.play('kaboom', 10, false, true);
					enemy.kill();
				}

		}




	}

		var gameOverScreen = function(game){};
gameOverScreen.prototype = {
}
