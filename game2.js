// This example uses the Phaser 2.2.2 framework

// Copyright © 2014 John Watson
// Licensed under the terms of the MIT License

// variables

var game;
var bullets;
var nextFire = 0;
var ship,turret;
var controlShip = false;
var x;
var y;
var pi,points,path;
var weapon,weapon2;
var enemies;
var alien;
var bulletOffset;
var bulletSpeed;
var bulletFireRate;
var bulletAngle;
var explosions;


window.onload = function() {
	game = new Phaser.Game(960, 1680, Phaser.AUTO, "game");
		game.state.add('game', GameState, true);
}

/*============THE GAME BIT=====================*/

var GameState = function(game) {
};

// Load images and sounds
GameState.prototype.preload = function() {
		this.game.load.image('console_player', 'console_player.png');
		this.game.load.image('ship_player', 'ship_player.png');
		this.game.load.image('console_enemy', 'console_enemy.png');
		this.game.load.image('ship_enemy', 'ship_enemy.png');
    this.game.load.image('bullet', 'bullet.png'); // /assets/gfx/bullet.png
};

// Setup the example
GameState.prototype.create = function() {
    // Set stage background color
		game.stage.backgroundColor = "#000000";

    // Define constants
    this.SHOT_DELAY = 60; // milliseconds (10 bullets/second)
    this.BULLET_SPEED = 480; // pixels/second
    this.NUMBER_OF_BULLETS = 5;

    // Create an object representing our gun
    this.gun = this.game.add.sprite(320, 460, this.game.height/1, 'bullet');

    // Set the pivot point to the center of the gun
    this.gun.anchor.setTo(0.5, 0.5);

    // Create an object pool of bullets
    this.bulletPool = this.game.add.group();
    for(var i = 0; i < this.NUMBER_OF_BULLETS; i++) {
        // Create each bullet and add it to the group.
        var bullet = this.game.add.sprite(0, 0, 'bullet');
        this.bulletPool.add(bullet);

        // Set its pivot point to the center of the bullet
        bullet.anchor.setTo(0.5, 0.5);

        // Enable physics on the bullet
        this.game.physics.enable(bullet, Phaser.Physics.ARCADE);

        // Set its initial state to "dead".
        bullet.kill();

				/*=====create a player console=======*/
				ship = this.add.sprite(40, 440, 'console_player');

				/*=====create a player ship=======*/
				 ship = this.add.sprite(200, 440, 'ship_player');

				 /*=====create a player console=======*/
				 ship = this.add.sprite(760, 440, 'console_enemy');

				 /*=====create a player ship=======*/
					ship = this.add.sprite(600, 440, 'ship_enemy');


		}
};

GameState.prototype.shootBullet = function() {
    // Enforce a short delay between shots by recording
    // the time that each bullet is shot and testing if
    // the amount of time since the last shot is more than
    // the required delay.
    if (this.lastBulletShotAt === undefined) this.lastBulletShotAt = 0;
    if (this.game.time.now - this.lastBulletShotAt < this.SHOT_DELAY) return;
    this.lastBulletShotAt = this.game.time.now;

    // Get a dead bullet from the pool
    var bullet = this.bulletPool.getFirstDead();

    // If there aren't any bullets available then don't shoot
    if (bullet === null || bullet === undefined) return;

    // Revive the bullet
    // This makes the bullet "alive"
    bullet.revive();

    // Bullets should kill themselves when they leave the world.
    // Phaser takes care of this for me by setting this flag
    // but you can do it yourself by killing the bullet if
    // its x,y coordinates are outside of the world.
    bullet.checkWorldBounds = true;
    bullet.outOfBoundsKill = true;

    // Set the bullet position to the gun position.
    bullet.reset(this.gun.x, this.gun.y);

    // Shoot it
    bullet.body.velocity.x = this.BULLET_SPEED;
    bullet.body.velocity.y = 0;
};

// The update() method is called every frame
GameState.prototype.update = function() {
    // Shoot a bullet
    if (this.game.input.activePointer.isDown) {
        this.shootBullet();
    }
};
